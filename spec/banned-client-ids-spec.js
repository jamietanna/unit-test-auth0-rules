var fs = require('fs');

// parentheses required to make it a callable function. Only read it once for a
// slightly lower cost of I/O read usage
var contents = "(" + fs.readFileSync('banned-client-ids.js') + ")";

// required class to inject into the Rule for testing error states
function UnauthorizedError(message) {
	this.message = message;
}

// because we need to hook in our global `configuration` variable, as well as
// mock out our external dependencies such as other modules
function execute_rule(user, context, callback, configuration) {
	// `eval` is required so the `configuration` variable is set correctly for
	// each execution of the Rule
	var rule = eval(contents);
	// return to the caller anything the rule has returned, although any return
	// values must be actually passed through `callback`
	return rule(user, context, callback);
}

describe('banned-client-ids.js', function() {
	// these variables to be cleared before each and every test (rather than
	// each `describe` block), so has to be in a `beforeEach` keep these three
	// set (and cleared) for each iteration
	var user = {};
	var context = {};
	var configuration = {};
	var callback = null;

	beforeEach(function() {
		user = {};
		context = {};
		configuration = {};
		callback = jasmine.createSpy('callback(a, b, c)')
			.and
			.callFake(function() {
				return 'callback';
			});
	});

	it('throws error if client ID is banned', function() {
		context = {
			clientID: 'BANNED_CLIENT_ID'
		};

		var ret = execute_rule(user, context, callback, configuration);
		expect(ret).toBe('callback');

		expect(callback).toHaveBeenCalledWith(jasmine.any(UnauthorizedError));
		var callbackArgs = callback.calls.argsFor(0);
		var error = callbackArgs[0];
		expect(error.message).toBe('Access to this application has been temporarily revoked');;
	});

	it('succeeds if client ID is not banned', function() {
		context = {
			clientId: 'BANNED_CLIENT_ID'
		};
		var ret = execute_rule(user, context, callback, configuration);
		expect(ret).toBe(undefined);

		expect(callback).toHaveBeenCalledWith(null, {}, context);
	});
});
