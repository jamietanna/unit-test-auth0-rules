var fs = require('fs');
var mock = require('mock-require');

// parentheses required to make it a callable function. Only read it once for a
// slightly lower cost of I/O read usage
var contents = "(" + fs.readFileSync('notify-slack.js') + ")";

// because we need to hook in our global `configuration` variable, as well as
// mock out our external dependencies such as other modules
function execute_rule(user, context, callback, configuration) {
	// any modules that we test should be suitably mocked, so we don't need to
	// rely on their implementations in this unit test
	mock('slack-notify', function(SLACK_HOOK) {
		// verify we have the correct `SLACK_HOOK` called by our Rule
		expect(SLACK_HOOK).toBe(configuration.SLACK_HOOK);
		return fakeSlackNotify;
	});

	// `eval` is required so the `configuration` variable is set correctly for
	// each execution of the Rule
	var rule = eval(contents);
	// return to the caller anything the rule has returned, although any return
	// values must be actually passed through `callback`
	return rule(user, context, callback);
}

var fakeSlackNotify = null;

describe('notify-slack.js', function() {
	// these variables to be cleared before each and every test (rather than
	// each `describe` block), so has to be in a `beforeEach` keep these three
	// set (and cleared) for each iteration
	var user = {};
	var context = {};
	var configuration = {};
	var callback = null;

	beforeEach(function() {
		user = {};
		context = {};
		configuration = {};
		callback = jasmine.createSpy('callback(a, b, c)')
			.and
			.callFake(function() {
				return 'callback';
			});
		fakeSlackNotify = jasmine.createSpyObj('slack-notify', ['success']);
	});

	describe('does not notify slack', function() {
		it('if user has logged in more than once', function() {
			context = {
				stats: {
					loginsCount: 2
				}
			};
			var ret = execute_rule(user, context, callback);
			expect(ret).toBe('callback');
			expect(callback).toHaveBeenCalledWith(null, user, context);

			expect(fakeSlackNotify.success).toHaveBeenCalledTimes(0);
		});

		it('if login is via OAuth2 refresh token', function() {
			context = {
				protocol: 'oauth2-refresh-token',
				stats: {
				}
			};
			var ret = execute_rule(user, context, callback);
			expect(ret).toBe('callback');
			expect(callback).toHaveBeenCalledWith(null, user, context);

			expect(fakeSlackNotify.success).toHaveBeenCalledTimes(0);
		});
	});

	describe('notifies slack', function() {
		it('if login count is 0', function() {
			configuration = {
				SLACK_HOOK: 'https://example.com/slack'
			};
			user = {
				email: 'doesnt@matter',
				name: 'John Smith'

			};
			context = {
				protocol: 'oauth2-login',
				stats: {
					loginsCount: 0
				}
			};
			var ret = execute_rule(user, context, callback, configuration);
			expect(ret).toBe(undefined);

			// note that we don't care in this case how it was called, just that
			// we do have a call
			expect(fakeSlackNotify.success).toHaveBeenCalledTimes(1);
			expect(callback).toHaveBeenCalledWith(null, user, context);
		});

		it('if login count is 1', function() {
			configuration = {
				SLACK_HOOK: 'https://example.com/slack'
			};
			user = {
				email: 'doesnt@matter',
				name: 'John Smith'

			};
			context = {
				protocol: 'oauth2-login',
				stats: {
					loginsCount: 1
				}
			};
			var ret = execute_rule(user, context, callback, configuration);
			expect(ret).toBe(undefined);

			// note that we don't care in this case how it was called, just that
			// we do have a call
			expect(fakeSlackNotify.success).toHaveBeenCalledTimes(1);
			expect(callback).toHaveBeenCalledWith(null, user, context);
		});

		it('with the user\'s name if `user.name` is set', function() {
			configuration = {
				SLACK_HOOK: 'https://example.com/slack'
			};
			user = {
				email: 'user@example.com',
				name: 'wibble'

			};
			context = {
				protocol: 'oauth2-login',
				stats: {
					loginsCount: 0
				}
			};
			var ret = execute_rule(user, context, callback, configuration);
			expect(ret).toBe(undefined);

			expect(fakeSlackNotify.success).toHaveBeenCalledWith({
				text: 'New User: wibble (user@example.com)',
				channel: '#some_channel'
			});
			expect(callback).toHaveBeenCalledWith(null, user, context);
		});

		it('with the user\'s email if `user.name` is not set', function() {
			configuration = {
				SLACK_HOOK: 'https://example.com/slack'
			};
			user = {
				email: 'user@example.com',
			};
			context = {
				protocol: 'oauth2-login',
				stats: {
					loginsCount: 0
				}
			};
			var ret = execute_rule(user, context, callback, configuration);
			expect(ret).toBe(undefined);

			expect(fakeSlackNotify.success).toHaveBeenCalledWith({
				text: 'New User: user@example.com (user@example.com)',
				channel: '#some_channel'
			});
			expect(callback).toHaveBeenCalledWith(null, user, context);
		});
	});
});
