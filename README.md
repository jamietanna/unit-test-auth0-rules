# Unit Testing an Auth0 Rule

This repo has two Auth0 Rules with associated unit tests written with the [Jasmine unit testing framework](http://jasmine.github.io/).

This has been tested with Node v11.1.0.

Once you have cloned this repo, you can `npm install` its dependencies:

```
$ npm install
npm WARN unit-test-auth0-rules No description
npm WARN unit-test-auth0-rules No repository field.
npm WARN unit-test-auth0-rules No license field.

removed 64 packages and audited 20 packages in 1.603s
found 0 vulnerabilities
```

Followed by running the jasmine tests by running `npm test`:

```
$ npm test

> @ test /home/jamie/workspaces/auth0/unit-test-auth0-rules
> jasmine

Randomized with seed 93142
Started
........

8 specs, 0 failures
Finished in 0.019 seconds
Randomized with seed 93142 (jasmine --random=true --seed=93142)
```

## `notify-slack.js`

This Auth0 rule notifies a Slack channel when a user logs in for the first time. It shows how to mock out external dependencies (the `slack-notify` library) as well as hooking in Auth0 configuration.

## `banned-client-ids.js`

This Auth0 rule shows an error scenario where we do not want a certain OAuth2 Client ID to be allowed access.
